<?php
/**
 * Created by PhpStorm.
 * User: l2cri
 * Date: 10.08.16
 * Time: 20:25
 */

namespace  L2cri\Buy;

use \CSaleBasket;


class Product
{
    public function add($id,$quantity = 1,$props = array()){

        if (!\CModule::IncludeModule("sale")) return false;

        $existBasket = self::check($id);

        if($existBasket) {
            //var_dump($existBasket,$props);
            return Services::add($id,$props);
        }
        else{
            $arElement = Iblock\Element::getByID($id,array('ID','NAME','DETAIL_PAGE_URL'));

            $rsServices = new  Iblock\Element();

            $rsServices->getList(array('ID'=>$props),array('ID','NAME'));

            $arProps = $rsServices->getProps();
            $propSumPrice = $rsServices->getAllSumm();

            $arFields = array(
                "PRODUCT_ID" => intval($arElement['ID']),
                "PRODUCT_PRICE_ID" => 0,//$arElement['PRICE']['ID'],
                "PRICE" => $propSumPrice + $arElement['PRICE']['DISCOUNT_PRICE'],
                "CURRENCY" => "RUB",
                "QUANTITY" => intval($quantity),
                "LID" => LANG,
                "DELAY" => "N",
                "CAN_BUY" => "Y",
                "NAME" => $arElement['NAME'],
                "CALLBACK_FUNC" => "BasketPropsRecalculate",
                "MODULE" => "l2cri.buy",
                "DETAIL_PAGE_URL" => $arElement['DETAIL_PAGE_URL']
            );

            $arFields["PROPS"] = $arProps;

            return CSaleBasket::add($arFields);
        }
    }

    public function check($product_id){

        if (!\CModule::IncludeModule("sale")) return false;

        $resBasket = CSaleBasket::GetList(
            array(),
            array(
                "PRODUCT_ID" => intval($product_id),
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array('ID','PRODUCT_ID','QUANTITY')
        );

        if($ar = $resBasket->Fetch()) {

            $arBasket = $ar;
            $arBasket['PRICE'] = Iblock\Element::getPrice($ar['PRODUCT_ID']);
            $arBasket['PROPS_PRICE']  = Services::getPropBasket($ar['ID']);
        }

        return $arBasket;
    }

    public function checkByBasketID($basketID){

        if (!\CModule::IncludeModule("sale")) return false;

        $resBasket = CSaleBasket::GetList(
            array(),
            array(
                "ID" => intval($basketID)
            ),
            false,
            false,
            array('ID','PRODUCT_ID','QUANTITY')
        );

        if($ar = $resBasket->Fetch()) {

            $arBasket = $ar;
            $arBasket['PRICE'] = Iblock\Element::getPrice($ar['PRODUCT_ID']);
            $arBasket['PROPS_PRICE']  = Services::getPropBasket($ar['ID']);
        }

        return $arBasket;
    }

    public function delete($id){
        $existBasket = self::check($id);

        if($existBasket){
            return CSaleBasket::Delete($existBasket['ID']);
        }
    }
}