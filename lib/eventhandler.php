<?php
/**
 * Created by PhpStorm.
 * User: l2cri
 * Date: 10.08.16
 * Time: 20:25
 */

namespace  L2cri\Buy;

use \CSaleBasket;


class EventHandler
{
    function bxModifySaleMails($orderID, &$eventName, &$arFields)
    {

        $str = '<table cellpadding="0" width="100%" cellspacing="0" style="width:100%">';

        if (!\CModule::IncludeModule("sale")) return false;

        //Get basket
        if ((int)$orderID > 0) {
            $dbBasket = CSaleBasket::GetList(
                array(),
                array("ORDER_ID" => $orderID),
                $arGroupBy = false,
                $arNavStartParams = false,
                $arSelectFields = array(
                    "ID",
                    "NAME",
                    "PRICE",
                    "CURRENCY",
                    "QUANTITY",
                    "DETAIL_PAGE_URL"
                )
            );
            $str .= '
            <tr>
                <td align="center" style="border:1px solid #dddddd; padding:10px 15px; border-bottom:none"><b>Наименование</b></td>
                <td align="center" style="border:1px solid #dddddd; padding:10px 15px; border-bottom:none; border-left:none; width:50px"><b>Кол-во</b></td>
                <td align="center" style="border:1px solid #dddddd; padding:10px 15px; border-bottom:none; border-left:none"><b>Услуги</b></td>
                <td align="center" style="width:150px; border:1px solid #dddddd; padding:10px 15px; border-bottom:none; border-left:none"><b>Стоимость</b></td>
            </tr>
            ';
            while ($arBasket = $dbBasket->Fetch()) {
                $arProps = Services::getPropBasket($arBasket['ID']);

                $propSrt = '';

                foreach ($arProps['ITEMS'] as $prop) {

                    if ($prop['VALUE'] <= 0) {
                        $prop['VALUE'] = 'бесплатно';
                    } else {
                        $prop['VALUE'] = number_format($prop['VALUE'], 0, ',', ' ') . ' руб';
                    }

                    $propSrt .= '<p style="font-size:13px;padding: 0 0 5px;margin: 0"><b>' .$prop['NAME'] . '</b> - ' . $prop['VALUE'] . '</p>';
                }

                $str .= '
                <tr>
                    <td style="border:1px solid #dddddd; padding:10px 15px"><a href="http://'.SITE_SERVER_NAME. $arBasket["DETAIL_PAGE_URL"] . '" style="color:#1e83e2">' . $arBasket["NAME"] . '</a></td>
                    <td align="center" style="border:1px solid #dddddd; padding:10px 15px; border-left:none">' . $arBasket["QUANTITY"] . '</td>
                    <td style="border:1px solid #dddddd; padding:10px 15px 5px; border-left:none"> ' . $propSrt . '</td>
                    <td align="center" style="border:1px solid #dddddd; padding:10px 15px; border-left:none">' . SaleFormatCurrency($arBasket["PRICE"], $arBasket["CURRENCY"]) . '</td>
                </tr>
                ';
            }
            $str .= '
            <tr align="right">
                <td colspan="3" style="border:1px solid #dddddd;border-top:none; padding:10px 15px;"><b>Cумма</b> </td>
                <td align="center" style="border:1px solid #dddddd;padding:10px 15px;border-top:none; border-left:none;"><b>'.$arFields["PRICE"].'<b></td>
            </tr>
            </table>
            ';
        }
        //rewrite
        $arFields["ORDER_LIST"] = $str;
    }
}