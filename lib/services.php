<?php
/**
 * Created by PhpStorm.
 * User: l2cri
 * Date: 10.08.16
 * Time: 20:27
 */

namespace L2cri\Buy;

use \CSaleBasket;

class Services
{
    public function add($productID,$serviceID,$basketID = false) {

        if (!\CModule::IncludeModule("sale")) return false;
        if (!\CModule::IncludeModule("catalog")) return false;

        if(!$basketID){
            $existBasket = Product::check($productID);
        } else {
            $existBasket = Product::checkByBasketID($basketID);
        }

        $propsIDS = $existBasket['IDS'];

        if(count($propsIDS)<= 0 || !in_array($serviceID,$propsIDS)){

            $rsServices = new  Iblock\Element();

            $rsServices->getList(array('ID'=>$serviceID),array('ID','NAME'));

            $arProps = $rsServices->getProps();

            $arFields["PRICE"] = $rsServices->getAllSumm() + $existBasket['PRICE'];
            $arFields["PROPS"] = $arProps;

            return CSaleBasket::Update($basketID, $arFields);
        }
    }

    public function del($productID,$serviceID,$basketID) {
        if (!\CModule::IncludeModule("sale")) return false;

        if(!$basketID){
            $existBasket = Product::check($productID);
        } else {
            $existBasket = Product::checkByBasketID($basketID);
        }

        $arProps =  $existBasket['PROPS_PRICE']['ITEMS'];

        $propsIDS = $existBasket['PROPS_PRICE']['IDS'];

        if(count($propsIDS)== 0 || in_array($serviceID,$propsIDS)){



            foreach($arProps as $key=> $prop){
                if($prop['CODE'] == $serviceID){
                    unset($arProps[$key]);
                    $minusSumm = $arProps['VALUE'];
                    break;
                }
            }

            $arFields["PRICE"] = $existBasket['PROPS_PRICE']['SUMM'] + $existBasket['PRICE'] - $minusSumm;
            $arFields["PROPS"] = array();
            CSaleBasket::Update($basketID, $arFields);
            $arFields["PROPS"] =  array_values($arProps);

            return CSaleBasket::Update($basketID, $arFields);
        }
    }

    public function getPropBasket ($basket_id){
        $summProps = 0;
        $arServices = array();
        $arServicesIDS = array();

        $db_res = \CSaleBasket::GetPropsList(
            array(
                "SORT" => "ASC",
                "NAME" => "ASC"
            ),
            array("BASKET_ID" => $basket_id),false,false,array('NAME','VALUE','CODE','SORT')
        );
        while ($ar_res = $db_res->Fetch())
        {
            $summProps+= floatval($ar_res["VALUE"]);
            $arServicesIDS[] = intval($ar_res['CODE']);
            $arServices[] = $ar_res;
        }

        return array('SUMM'=>$summProps,'IDS'=>$arServicesIDS,'ITEMS'=>$arServices);
    }
}