<?php
/**
 * Created by PhpStorm.
 * User: l2cri
 * Date: 10.08.16
 * Time: 20:49
 */

namespace L2cri\Buy\Iblock;

use \CIBlockElement;
use \CCatalogProduct;
use \CCatalogSku;


class Element
{

    private $summPrice = 0;
    private $arProps = array();
    const OfferIblockID = 15;

    public static function getByID($id,$arSelect = array()){

        if(!\CModule::IncludeModule('iblock')) return false;

        $arFilter = array('=ID' => intval($id));


        $arElement = CIBlockElement::GetList(array(),$arFilter,false,false,$arSelect)->GetNext(true,false);

        if(!$arElement) return false;

        $arElement['PRICE'] = self::getArPrice($arElement['ID']);

        return  $arElement;
    }

    public static function getElementsProps($id,$code_prop){
        if(!\CModule::IncludeModule('iblock')) return false;

        $product_id = self::isOffer($id);

        $arFilter = array('=ID' => intval($product_id));

        $rs =  CIBlockElement::GetList(array(),$arFilter);

        if( $rsElement =$rs->GetNextElement(true,false)) $arElement = $rsElement->GetProperty($code_prop);

        if(is_array($arElement['VALUE']) && count($arElement['VALUE'])) {

            $rsServices = new Element();

            $rsServices->getList(array('ID'=>$arElement['VALUE']),array('ID','NAME'));

            $arProps = $rsServices->getProps();

            return $arProps;

        }

    }

    // TODO переписать в другой класс так как работает конкретно с сервисами
    public function getList($arFilter = array(),$arSelect = array()){
        if(!\CModule::IncludeModule('iblock')) return false;

        $arElements = array();

        if(is_null($arFilter['ID'])
            || (is_array($arFilter['ID']) && count($arFilter['ID']) == 0)
        ) return false;

        $res = CIBlockElement::getList(array(),$arFilter,false,false,$arSelect);

        while($rsElement = $res->GetNext(true,false)){
            $arElements[$rsElement['ID']] = $rsElement;
            $price = 0;

            if($arPrice = $this->getArPrice($rsElement['ID'])){
                $price = floatval($arPrice['DISCOUNT_PRICE']);
            }

            $this->summPrice += $price;

            $arElements[$rsElement['ID']]['PRICE'] = $price;

            $this->arProps[] = array(
                'NAME'=>$rsElement['NAME'],
                'CODE' => $rsElement['ID'],
                'VALUE' => $price
            );
        }

    }

    public static function isOffer($id) {
        if(!\CModule::IncludeModule('catalog')) return false;

        $mxResult = CCatalogSku::GetProductInfo($id,self::OfferIblockID);

        if(is_array($mxResult)) return $mxResult['ID'];
        else return $id;
    }

    public function getProps(){
        return $this->arProps;
    }

    private function getArPrice($productID){

        if (!\CModule::IncludeModule("catalog")) return false;

        global $USER;
        $arPrice = CCatalogProduct::GetOptimalPrice($productID, 1, $USER->GetUserGroupArray());

        return $arPrice['RESULT_PRICE'];
    }

    public function getPrice($productID){
        if($arPrice = self::getArPrice($productID)) return floatval($arPrice['DISCOUNT_PRICE']);
    }

    public function getAllSumm(){
        return $this->summPrice;
    }

}