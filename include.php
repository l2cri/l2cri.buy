<?php

function BasketPropsRecalculate($PRODUCT_ID, $QUANTITY = 0)
{
    /** DEFAULT **/
    if($QUANTITY<= 0) return array();
    $arResult['CAN_BUY'] = "Y";
    $arResult['PRODUCT_PRICE_ID'] = $PRODUCT_ID;
    $arResult['QUANTITY'] = $QUANTITY;
    $arResult['CURRENCY'] = 'RUB';
    $arResult['LID'] = LANG;

    $existBasket = L2cri\Buy\Product::check($PRODUCT_ID);

    if($existBasket) {
        $arResult['PRICE'] = $existBasket['PRICE'] + $existBasket['PROPS_PRICE']['SUMM'];
    }

    return $arResult;
}