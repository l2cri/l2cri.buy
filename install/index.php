<?
Class l2cri_buy extends CModule
{
    var $MODULE_ID = "l2cri.buy";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function l2cri_buy()
    {
        $path = str_replace("\\", "/", __FILE__);
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = "Покупка сопуствующих сервисов";
        $this->MODULE_DESCRIPTION = "Пересчет цен в зависимости от приобретенных услуг, записываемых в свойство товара, в  корзине";
        $this->PARTNER_NAME = "l2cri";
        $this->PARTNER_URI = 'http://vk.com/l2cri';
    }

    function InstallFiles()
    {

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/l2cri.buy/install/ajax", $_SERVER["DOCUMENT_ROOT"]."/ajax", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/l2cri.buy/install/personal", $_SERVER["DOCUMENT_ROOT"]."/personal", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/l2cri.buy/install/templates", $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates", true, true);
        return true;
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;

        // Install events;
        RegisterModuleDependences("main", "OnPageStart","l2cri.buy","loadModule", "loadBuyLib", 1,$DOCUMENT_ROOT."/bitrix/modules/l2cri.buy/loadModule.php");
        RegisterModuleDependences("sale", "OnOrderNewSendEmail","l2cri.buy",'\L2cri\Buy\EventHandler', "bxModifySaleMails", 1);

        RegisterModule($this->MODULE_ID);
        $this->InstallFiles();
        $APPLICATION->IncludeAdminFile("Установка модуля Покупка сопуствующих сервисов", $DOCUMENT_ROOT."/bitrix/modules/l2cri.buy/install/step.php");
        return true;
    }

    function DoUninstall()
    {

        global $DOCUMENT_ROOT, $APPLICATION;

        // Unstall events;
        UnRegisterModuleDependences("main", "OnPageStart","l2cri.buy","loadModule", "loadBuyLib",$DOCUMENT_ROOT."/bitrix/modules/l2cri.buy/loadModule.php");
        UnRegisterModuleDependences("sale", "OnOrderNewSendEmail","l2cri.buy",'\L2cri\Buy\EventHandler', "bxModifySaleMails");

        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля Покупка сопуствующих сервисов", $DOCUMENT_ROOT."/bitrix/modules/l2cri.buy/install/unstep.php");
        return true;
    }
}