$(document).ready(function($) {

    /***Callback***/
    $('.callback_anch').click(function(e){
        e.preventDefault();
        $('.callback_body').css({'display':'block'});
        $('.callback').css({'display':'block'});
    });
    $('.callback_close, .callback_body').click(function(e){
        e.preventDefault();
        $('.callback_body').css({'display':'none'});
        $('.callback').css({'display':'none'});
    });


    /***Tabs***/
    $('ul.TabsLink').on('click', 'li:not(.current)', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.TabsInfo').find('div.BlockBox').eq($(this).index()).fadeIn(150).siblings('div.BlockBox').hide();
    })

    var tabIndex = window.location.hash.replace('#tab','')-1;
    if (tabIndex != -1) $('ul.TabsLink li').eq(tabIndex).click();

    $('a[href*=#tab]').click(function() {
        var tabIndex = $(this).attr('href').replace(/(.*)#tab/, '')-1;
        $('ul.TabsLink li').eq(tabIndex).click();
    });


    /***Show_delay***/
    var currPage = window.location.pathname;
    var delayIndex = window.location.search;
    if((currPage == '/personal/cart/') && (document.getElementById('basket_items_delayed')) && (delayIndex == '?delay=Y')) {
        $('#basket_items_delayed').show();
        $('#basket_items_list').hide();
    } else {
        $('#basket_items_delayed').hide();
        $('#basket_items_list').show();

    }


    /*** recount price in detail product page **/
    var listServices = $('.Services'),
        productPrice = $('.catalog-detail-price .Price span');

    if(listServices.length){
        listServices.on('click','.Item input', function(){

            var arPrices = $('.Services .Item input'),
                summ = 0;

            if(productPrice.length) summ = productPrice.data('price');


            if(arPrices.length){
                arPrices.each(function(){
                    if($(this).is( ":checked" )){
                        summ+= parseFloat($(this).data('price'));
                    }
                });
            }

            if(productPrice.length) productPrice.text(addSpaces(summ));

        })
    }

});

function addSpaces(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}

function GatherDataFromLocation(locationClass) {
    //User inputs array
    var arUserInput = new Array();
    //getting input values
    var formInputs = $(locationClass).find('input');
    var arformInputs = $.makeArray(formInputs);
    for (var i = 0; i < arformInputs.length; i++) {
        if (arformInputs[i].type != 'button') {
            if((arformInputs[i].type == 'checkbox' || arformInputs[i].type == 'radio') && arformInputs[i].checked == true){
                inputName = arformInputs[i].name;
                arUserInput[inputName] = arformInputs[i].value;
            }
            else if(arformInputs[i].type != 'checkbox' && arformInputs[i].type != 'radio'){
                inputName = arformInputs[i].name;
                arUserInput[inputName] = arformInputs[i].value;
            }
        }
    }
    //get textareas values
    var formTextareas = $(locationClass).find('textarea');
    var arformTextareas = $.makeArray(formTextareas);
    for (var j = 0; j < arformTextareas.length; j++) {
        textareaName = arformTextareas[j].name;
        arUserInput[textareaName] = arformTextareas[j].value;
    }
    //
    //get select list
    var formSelects = $(locationClass).find('select');
    var arformSelects = $.makeArray(formSelects);
    for (var k = 0; k < arformSelects.length; k++) {
        selectName = arformSelects[k].name;
        arUserInput[selectName] = arformSelects[k].value;
    }
    //
    var strUserInputData = '';
    for (keyVar in arUserInput){
        strUserInputData = strUserInputData + keyVar + '=' + arUserInput[keyVar] + '&'
    }
    return  strUserInputData;
}