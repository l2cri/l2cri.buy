<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bPriceType  = false;
$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;

if ($delayCount > 0):
?>
<div id="basket_items_delayed" class="bx_ordercart_order_table_container" style="display:none">
	<div class="CartInfo">
		<div class="CartList">
		<table id="delayed_items">
			<thead>
				<tr>
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
						if ($arHeader["name"] == '')
							$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
						if (in_array($arHeader["id"], array("TYPE"))) // some header columns are shown differently
						{
							$bPriceType = true;
							continue;
						}
						elseif ($arHeader["id"] == "PROPS")
						{
							$bPropsColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "DELAY")
						{
							continue;
						}
						elseif ($arHeader["id"] == "DELETE")
						{
							$bDeleteColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "WEIGHT")
						{
							$bWeightColumn = true;
						}

						if ($arHeader["id"] == "NAME"):
						?>
							<td class="ImageTD" id="col_<?=$arHeader["id"];?>">
							<td class="NameTD">
							<td class="PropsTD">
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="PriceTD">
						<?
						else:
						?>
							<td class="CountTD">
						<?
						endif;
						?>
							<?if($arHeader["name"]):?>
							<?=$arHeader["name"]; ?>
							<?else:?>
							Цвет
							<?endif;?>
							</td>
					<?
					endforeach;

					if ($bDeleteColumn || $bDelayColumn):
					?>
						<td class="custom"></td>
					<?
					endif;
					?>
				</tr>
			</thead>

			<tbody>
				<?
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

					if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y"):
				?>
					<tr id="<?=$arItem["ID"]?>">
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

							if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in columns in this template
								continue;

							if ($arHeader["id"] == "NAME"):
							?>
								<td class="ImageTD">
									<div class="bx_ordercart_photo_container">
										<?
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
											$url = $arItem["DETAIL_PICTURE_SRC"];
										else:
											$url = $templateFolder."/images/no_photo.png";
										endif;
										?>
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
											<div class="bx_ordercart_photo" style="background-image:url('<?=$url?>')"></div>
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
									</div>
									<?
									if (!empty($arItem["BRAND"])):
									?>
									<div class="bx_ordercart_brand">
										<img alt="" src="<?=$arItem["BRAND"]?>" />
									</div>
									<?
									endif;
									?>
								</td>
								<td class="NameTD item">
									<div class="bx_ordercart_itemtitle">
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
											<?=$arItem["NAME"]?>
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
									</div>
								</td>
								<td class="PropsTD">
									<?if($arItem['SERVICES']):?>
                                    <div class="Services">
                                        <?foreach($arItem['SERVICES'] as $arService):?>
											<div class="Item">
												<input <?if($arService['CHECKED']):?>checked="checked"<?endif;?> type="checkbox" id="<?=$arItem['ID']
												?>_<?=$arService['CODE']?>" data-basket="<?=$arItem['ID']?>" data-product="<?=$arItem['PRODUCT_ID']?>" value="<?=$arService['CODE']?>">

												<label for="<?=$arItem['ID']?>_<?=$arService['CODE']?>"><span><?=$arService['NAME']?></span></label>
												<?if(floatval($arService['VALUE']) > 0):?>
													<div class="Price"><span><?=number_format($arService['VALUE'], 0, ',', ' ')?></span> руб.</div>
												<?endif;?>
											</div>
										<?endforeach;?>
                                    </div>
                                    <?endif;?>
									<?
									if (is_array($arItem["SKU_DATA"])):
											foreach ($arItem["SKU_DATA"] as $propId => $arProp):

												// is image property
												$isImgProperty = false;
												foreach ($arProp["VALUES"] as $id => $arVal)
												{
													if (isset($arVal["PICT"]) && !empty($arVal["PICT"]))
													{
														$isImgProperty = true;
														break;
													}
												}
												$countValues = count($arProp["VALUES"]);
												$full = ($countValues > 5) ? "full" : "";

												if ($isImgProperty):
												?>
													<div class="bx_item_detail_scu_small_noadaptive <?=$full?>">

														<span class="bx_item_section_name_gray">
															<?=$arProp["NAME"]?>:
														</span>

														<div class="bx_scu_scroller_container">

															<div class="bx_scu">
																<ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>" style="width: 200%;margin-left:0%;">
																<?
																foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																	$selected = "";
																	foreach ($arItem["PROPS"] as $arItemProp):
																		if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																		{
																			if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
																				$selected = ' class="bx_active"';
																		}
																	endforeach;
																?>
																	<li style="width:10%;"<?=$selected?>>
																		<a href="javascript:void(0)" class="cnt"><span class="cnt_item" style="background-image:url(<?=$arSkuValue["PICT"]["SRC"]; ?>)"></span></a>
																	</li>
																<?
																endforeach;
																?>
																</ul>
															</div>

															<div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
															<div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
														</div>

													</div>
												<?
												else:
												?>
													<div class="bx_item_detail_size_small_noadaptive <?=$full?>">

														<!--<span class="bx_item_section_name_gray">
															<?=$arProp["NAME"]?>:
														</span>-->

														<div class="bx_size_scroller_container">
															<div class="bx_size">
																<ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>">
																	<?
																	foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																		$selected = "";
																		foreach ($arItem["PROPS"] as $arItemProp):
																			if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																			{
																				if ($arItemProp["VALUE"] == $arSkuValue["NAME"])
																					$selected = ' class="bx_active"';
																			}
																		endforeach;
																	?>
																	
																	<?if($selected):?>
																	<li>
																		<a href="javascript:void(0)" class="cnt" title="<?=$arSkuValue["NAME"]?>"><span><i class="Color<?=$arSkuValue["ID"]?>"></i></span></a>
																	</li>
																	<?endif;?>
																	<!--<li <?=$selected?>>
																			<a href="javascript:void(0);" class="cnt" title="<?=$arSkuValue["NAME"]?>"><span><i></i></span></a>
																	</li>-->
																	<?
																	endforeach;
																	?>
																</ul>
															</div>
															<div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
															<div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
														</div>

													</div>
												<?
												endif;
											endforeach;
									endif;
									?>
									<input type="hidden" name="DELAY_<?=$arItem["ID"]?>" value="Y"/>
								</td>
							<?
							elseif ($arHeader["id"] == "QUANTITY"):
							?>
								<td class="CountTD">
									<div class="Value">
										<?echo $arItem["QUANTITY"];
											if (isset($arItem["MEASURE_TEXT"]))
												echo "&nbsp;".$arItem["MEASURE_TEXT"];
										?>
									</div>
								</td>
							<?
							elseif ($arHeader["id"] == "PRICE"):
							?>
								<td class="PriceTD price">
									<?if (doubleval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
										<div class="current_price"><?=$arItem["PRICE_FORMATED"]?></div>
										<div class="old_price"><?=$arItem["FULL_PRICE_FORMATED"]?></div>
									<?else:?>
										<div class="current_price"><?=$arItem["PRICE_FORMATED"];?></div>
									<?endif?>

									<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
										<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
										<div class="type_price_value"><?=$arItem["NOTES"]?></div>
									<?endif;?>
								</td>
							<?
							elseif ($arHeader["id"] == "DISCOUNT"):
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?>
								</td>
							<?
							elseif ($arHeader["id"] == "WEIGHT"):
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<?=$arItem["WEIGHT_FORMATED"]?>
								</td>
							<?
							else:
							?>
								<td class="SummTD custom">
									<?=$arItem[$arHeader["id"]]?>
								</td>
							<?
							endif;
						endforeach;

						if ($bDelayColumn || $bDeleteColumn):
						?>
							<td class="IconsTD control">
								<div class="Button">
									<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["add"])?>"><?=GetMessage("SALE_ADD_TO_BASKET")?></a>
									<?if ($bDeleteColumn):?>
									<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="DelIcon"><?=GetMessage("SALE_DELETE")?></a>
									<?endif;?>
								</div>
							</td>
						<?
						endif;
						?>
					</tr>
					<?
					endif;
				endforeach;
				?>
			</tbody>

		</table>
	</div>
</div>
<?
else:
?>
<div id="basket_items_delayed">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_DELAY_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>