<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arParams['USE_COMPARE']) {
    $arResult['COMPARE_URL'] = htmlspecialchars($APPLICATION->GetCurPageParam("action=ADD_TO_COMPARE_LIST&id=".$arResult['ID'], array("action", "id")));
}

if(is_array($arResult["DETAIL_PICTURE"])) {
    $arFilter = '';
    if($arParams["SHARPEN"] != 0) {
        $arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
    }

    $arFileTmp = CFile::ResizeImageGet(
        $arResult['DETAIL_PICTURE'],
        array("width" => "435", "height" => "455"),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true, $arFilter
    );

    $arResult['DETAIL_IMG'] = array(
        'SRC' => $arFileTmp["src"],
        'WIDTH' => $arFileTmp["width"],
        'HEIGHT' => $arFileTmp["height"],
    );

    $arFileTmp_prev = CFile::ResizeImageGet(
        $arResult['DETAIL_PICTURE'],
        array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true, $arFilter
    );

    $arResult['PREVIEW_IMG'] = array(
        'SRC' => $arFileTmp_prev["src"],
        'WIDTH' => $arFileTmp_prev["width"],
        'HEIGHT' => $arFileTmp_prev["height"],
    );
}

if(is_array($arResult['MORE_PHOTO']) && count($arResult['MORE_PHOTO']) > 0) {
    unset($arResult['DISPLAY_PROPERTIES']['MORE_PHOTO']);

    foreach($arResult['MORE_PHOTO'] as $key => $arFile) {
        $arFilter = '';
        if($arParams["SHARPEN"] != 0) {
            $arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
        }
        $arFileTmp = CFile::ResizeImageGet(
            $arFile,
            array("width" => $arParams["DISPLAY_MORE_PHOTO_WIDTH"], "height" => $arParams["DISPLAY_MORE_PHOTO_HEIGHT"]),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true, $arFilter
        );

        $arFile['PREVIEW_WIDTH'] = $arFileTmp["width"];
        $arFile['PREVIEW_HEIGHT'] = $arFileTmp["height"];

        $arFile['SRC_PREVIEW'] = $arFileTmp['src'];
        $arResult['MORE_PHOTO'][$key] = $arFile;
    }
}

if($arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]):
    $obElement = CIBlockElement::GetByID($arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]);
    if($arEl = $obElement->GetNext())
        $rsFile = CFile::GetFileArray($arEl["PREVIEW_PICTURE"]);

    $arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"] = array(
        "SRC" => $rsFile["SRC"],
        'WIDTH' => $rsFile["WIDTH"],
        'HEIGHT' => $rsFile["HEIGHT"],
    );
endif;

if($arResult["PROPERTIES"]["SERVICES"]["VALUE"] && CModule::IncludeModule('l2cri.buy'))
{
    array();

    $rsServices = new  L2cri\Buy\Iblock\Element();

    $rsServices->getList(array('ID'=>$arResult["PROPERTIES"]["SERVICES"]["VALUE"]),array('ID','NAME'));

    $arProps = $rsServices->getProps();

    $arResult['SERVICES'] = $arProps;
}

$reviews_count = CIBlockElement::GetList(
    array(),
    array('IBLOCK_CODE' => 'comments_'.SITE_ID, 'PROPERTY_OBJECT_ID'=> $arResult["ID"], 'ACTIVE'=> 'Y'),
    array()
);
$arResult["REVIEWS"]["COUNT"] = $reviews_count;

$res = CIBlock::GetList(
    Array(),
    Array('TYPE' => 'catalog', 'SITE_ID' => SITE_ID, 'ACTIVE' => 'Y', "CODE" => 'comments_'.SITE_ID),
    true
);
$reviews_iblock = $res->Fetch();
$arResult["REVIEWS"]["IBLOCK_ID"] = $reviews_iblock["ID"];


/***OFFERS***/
if(is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) {

    $arIblockOfferProps = array();
    foreach($arResult["OFFERS"] as $arOffer) {
        foreach($arOffer["DISPLAY_PROPERTIES"] as $keyProp => $propOffer) {
            $arIblockOfferProps[$propOffer["CODE"]] = array(
                "CODE" => $propOffer["CODE"],
                "NAME" => $propOffer["NAME"]
            );
        }
    }

    foreach($arResult["OFFERS"] as $keyOffer => $arOffer) {
        /***PROPERTIES***/
        foreach($arIblockOfferProps as $key => $arCode) {
            /***KEYS***/
            $keys = array_keys($arIblockOfferProps);
            $current_key_index = array_search($key, $keys);
            $prev_key = false;
            $next_key = false;
            if(isset($keys[$current_key_index + -1]))
                $prev_key = $keys[$current_key_index + -1];
            if(isset($keys[$current_key_index + 1]))
                $next_key = $keys[$current_key_index + 1];

            $id = false;
            $PARENT_ID = false;
            $offer_id = false;

            if($prev_key == false && $next_key == false) {
                $id = false;
                $PARENT_ID = false;
                $offer_id = $arOffer['ID'];
            } elseif($prev_key == false && $next_key != false) {
                $id = $arOffer["DISPLAY_PROPERTIES"][$key]["VALUE"];
                $PARENT_ID = false;
                $offer_id = false;
            } elseif($prev_key != false && $next_key == false) {
                $id = false;
                $PARENT_ID = $arOffer['DISPLAY_PROPERTIES'][$prev_key]['VALUE'];
                $offer_id = $arOffer['ID'];
            }

            if('L' == $arOffer["DISPLAY_PROPERTIES"][$key]['PROPERTY_TYPE']) {
                $arIblockOfferProps[$key]["VALUES"][] = array(
                    'ID' => $id,
                    'PARENT_ID' => $PARENT_ID,
                    'OFFER_ID' => $offer_id,
                    'VALUE' => $arOffer["DISPLAY_PROPERTIES"][$key]['VALUE'],
                );
            }

            if('E' == $arOffer["DISPLAY_PROPERTIES"][$key]['PROPERTY_TYPE']) {
                if('COLOR' == $arOffer["DISPLAY_PROPERTIES"][$key]['CODE']) {
                    $obElement = CIBlockElement::GetByID($arOffer["DISPLAY_PROPERTIES"][$key]["VALUE"]);
                    if($arEl = $obElement->GetNext())

                        $arSelect = Array("ID", "NAME", "PROPERTY_HEX", "PROPERTY_PICT");
                    $arFilter = Array("IBLOCK_ID" => $arEl["IBLOCK_ID"], "ID"=> $arEl["ID"]);
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($ob = $res->GetNextElement()) {
                        $arFields = $ob->GetFields();
                        $arIblockOfferProps[$key]["VALUES"][] = array(
                            'ID' => $id,
                            'PARENT_ID' => $PARENT_ID,
                            'OFFER_ID' => $offer_id,
                            'VALUE' => $arFields["NAME"],
                            'HEX' => $arFields["PROPERTY_HEX_VALUE"],
                            'PICT' => CFile::ResizeImageGet(
                                $arFields["PROPERTY_PICT_VALUE"],
                                array("width" => 24, "height" => 24),
                                BX_RESIZE_IMAGE_PROPORTIONAL,
                                true
                            )
                        );
                    }
                } else {
                    $obElement = CIBlockElement::GetByID($arOffer["DISPLAY_PROPERTIES"][$key]['VALUE']);
                    if($arEl = $obElement->GetNext())
                        $arIblockOfferProps[$key]["VALUES"][] = array(
                            'ID' => $id,
                            'PARENT_ID' => $PARENT_ID,
                            'OFFER_ID' => $offer_id,
                            'VALUE' => $arEl['NAME'],
                        );
                }
            }
        }
        /***END_PROPERTIES***/

        /***DETAIL IMAGE***/
        if(isset($arOffer["DETAIL_PICTURE"])) {
            $arFilter = '';
            if($arParams["SHARPEN"] != 0) {
                $arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
            }

            $arFile = CFile::GetFileArray($arOffer["DETAIL_PICTURE"]);
            $arResult["OFFERS"][$keyOffer]['DETAIL_PICTURE'] = $arFile;

            $arFileTmp = CFile::ResizeImageGet(
                $arOffer['DETAIL_PICTURE'],
                array("width" => $arParams["DISPLAY_DETAIL_IMG_WIDTH"], "height" => $arParams["DISPLAY_DETAIL_IMG_HEIGHT"]),
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true, $arFilter
            );

            $arResult["OFFERS"][$keyOffer]['DETAIL_IMG'] = array(
                'SRC' => $arFileTmp["src"],
                'WIDTH' => $arFileTmp["width"],
                'HEIGHT' => $arFileTmp["height"],
            );

            $arFileTmp_prev = CFile::ResizeImageGet(
                $arOffer["DETAIL_PICTURE"],
                array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true, $arFilter
            );

            $arResult["OFFERS"][$keyOffer]["PREVIEW_IMG"] = array(
                "SRC" => $arFileTmp_prev["src"],
                'WIDTH' => $arFileTmp_prev["width"],
                'HEIGHT' => $arFileTmp_prev["height"],
            );
        }
        /***END_DETAIL_IMAGE***/
    }

    $arResult["SKU_PROPERTIES"] = $arIblockOfferProps;
}
/***END_OFFERS***/
?>