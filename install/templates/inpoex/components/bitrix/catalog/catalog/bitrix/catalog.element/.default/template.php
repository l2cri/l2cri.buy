<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$sticker = "";
if(array_key_exists("NEWPRODUCT", $arResult["PROPERTIES"]) && intval($arResult["PROPERTIES"]["NEWPRODUCT"]["PROPERTY_VALUE_ID"]) > 0) {
    $sticker .= "<span class='New'>New</span><br />";
}
if(array_key_exists("SALELEADER", $arResult["PROPERTIES"]) && intval($arResult["PROPERTIES"]["SALELEADER"]["PROPERTY_VALUE_ID"]) > 0) {
    $sticker .= "<span class='Hit'>Hit</span><br />";
}
if(array_key_exists("DISCOUNT", $arResult["PROPERTIES"]) && intval($arResult["PROPERTIES"]["DISCOUNT"]["PROPERTY_VALUE_ID"]) > 0) {
    $sticker .= "<span class='Sale'><i></i></span><br />";
}?>
    <div class="CatalogDetail catalog-detail" itemscope itemtype="http://schema.org/Product">
        <meta content="<?=$arResult["NAME"]?>" itemprop="name">
        <div class="ImageBlock catalog-detail-pictures">
            <?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                foreach($arResult["OFFERS"] as $key => $arOffer):?>
                    <div id="detail_picture_<?=$arOffer["ID"]?>" class="Images detail_picture <?=$arResult['ID']?> hidden">
                        <?if(is_array($arOffer['DETAIL_IMG'])):?>
                            <a class="catalog-detail-images" href="<?=$arOffer['DETAIL_PICTURE']['SRC']?>">
                                <span class="Image"><img src="<?=$arOffer['DETAIL_IMG']['SRC']?>" alt="<?=$arResult["NAME"]?>" width="<?=$arOffer['DETAIL_IMG']["WIDTH"]?>" height="<?=$arOffer['DETAIL_IMG']["HEIGHT"]?>"/></span>
                                <span class="Stiker"><?=$sticker?></span>
                            </a>
                        <?else:?>
                            <a class="catalog-detail-images" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
                                <span class="Image"><img src="<?=$arResult['DETAIL_IMG']['SRC']?>" alt="<?=$arResult["NAME"]?>" width="<?=$arResult['DETAIL_IMG']["WIDTH"]?>" height="<?=$arResult['DETAIL_IMG']["HEIGHT"]?>"/></span>
                                <span class="Stiker"><?=$sticker?></span>
                            </a>
                        <?endif;?>
                    </div>
                <?endforeach;
            else:
                if(is_array($arResult['DETAIL_IMG'])):?>
                    <div class="Images detail_picture">
                        <a rel="catalog-detail-images" class="catalog-detail-images" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
                            <span class="Image"><img src="<?=$arResult['DETAIL_IMG']['SRC']?>" alt="<?=$arResult["NAME"]?>" width="<?=$arResult['DETAIL_IMG']["WIDTH"]?>" height="<?=$arResult['DETAIL_IMG']["HEIGHT"]?>"/></span>
                            <span class="Stiker"><?=$sticker?></span>
                        </a>
                    </div>
                <?else:?>
                    <div class="Images detail_picture">
                        <span class="Image"><img src="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" width="150px" height="150px" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /></span>
                        <span class="Stiker"><?=$sticker?></span>
                    </div>
                <?endif;
            endif;
            /*if(!empty($arResult["PROPERTIES"]['VIDEO']) || count($arResult["MORE_PHOTO"])>0):?>
                <div class="clr"></div>
                <div class="more_photo">
                    <ul>
                        <?if(!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])):?>
                            <li class="catalog-detail-video">
                                <a rel="catalog-detail-video" class="catalog-detail-images" href="#video">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/video.jpg" width="81px" height="81px" alt="<?=$arResult["NAME"]?>"/>
                                </a>
                                <div id="video">
                                    <?=$arResult["PROPERTIES"]["VIDEO"]["~VALUE"]["TEXT"];?>
                                </div>
                            </li>
                        <?endif;
                        if(count($arResult["MORE_PHOTO"])>0):
                            foreach($arResult["MORE_PHOTO"] as $PHOTO):?>
                                <li>
                                    <a rel="catalog-detail-images" class="catalog-detail-images" href="<?=$PHOTO["SRC"]?>">
                                        <img src="<?=$PHOTO["SRC_PREVIEW"]?>" width="<?=$PHOTO["PREVIEW_WIDTH"]?>" height="<?=$PHOTO["PREVIEW_HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" />
                                    </a>
                                </li>
                            <?endforeach;
                        endif;?>
                    </ul>
                </div>
            <?endif*/?>
        </div>
        <div class="DescBlock">
            <?$frame = $this->createFrame("catalog-detail")->begin();?>
            <script type="text/javascript">
                $(document).ready(function() {
					$(".catalog-detail-images").fancybox();
                    $(".add2basket_form").submit(function() {

                        var form = $(this);

                        imageItem = form.find("#item_image").attr('value');
                        $('#addItemInCart .item_image').attr('src', imageItem);

                        titleItem = form.find("#item_title").attr('value');
                        $('#addItemInCart .item_title').text(titleItem);

                        articulItem = form.find("#item_articul").attr('value');
                        $('#addItemInCart .item_articul').text(articulItem);

                        descItem = form.find("#item_desc").attr('value');
                        $('#addItemInCart .item_desc').text(descItem);

                        countItem = form.find(".quantity").attr('value');
                        $('#addItemInCart .item_count').text(countItem);

                        var ModalName = $('#addItemInCart');
                        CentriredModalWindow(ModalName);
                        OpenModalWindow(ModalName);

                        var data =  GatherDataFromLocation($('.Services')) + $(this).serialize();


                        $.post($(this).attr("action"), data, function(data) {
                            try {
                                $.post("/ajax/basket_line.php", function(data) {
                                    $("span#cart_line").replaceWith(data);
                                });
                                $.post("/ajax/delay_line.php", function(data) {
                                    $("#delay").replaceWith(data);
                                });
                                form.children("#add2basket").addClass("hidden");
                                form.children(".result").removeClass("hidden");
                            } catch (e) {}
                        });
                        return false;
                    });
                });
            </script>
            <?$this->SetViewTarget('buyblock');?>
            <div class="BuyBlock price_buy_detail" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <?if($arResult['PROPERTIES']['ARTNUMBER']['VALUE']):?>
                    <div class="Articul">Код товара: <?=$arResult['PROPERTIES']['ARTNUMBER']['VALUE']?></div>
                <?endif?>
                <div class="PriceBlock catalog-detail-price">
                    <?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                        foreach($arResult["OFFERS"] as $key => $arOffer):
                            foreach($arOffer["PRICES"] as $code => $arPrice):
                                if($arPrice["MIN_PRICE"] == "Y"):
                                    if($arPrice["CAN_ACCESS"]):

                                        $price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
                                        if(empty($price["THOUSANDS_SEP"])):
                                            $price["THOUSANDS_SEP"] = " ";
                                        endif;
                                        $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                        if($arPrice["VALUE"] == 0):?>
                                            <meta content="<?=GetMessage('CATALOG_ASK_PRICE')?>" itemprop="price">
                                            <div id="detail_price_<?=$arOffer["ID"]?>" class="detail_price <?=$arResult["ID"]?> hidden">
                                                <a class="ask_price_anch" id="ask_price_anch_<?=$arOffer['ID']?>" href="#"><?=GetMessage('CATALOG_ASK_PRICE')?></a>
                                                <?$properties = false;
                                                foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                    $properties[] = $propOffer["NAME"].": ".strip_tags($propOffer["DISPLAY_VALUE"]);
                                                }
                                                $properties = implode("; ", $properties);
                                                if(!empty($properties)):
                                                    $offer_name = $arResult["NAME"]." (".$properties.")";
                                                else:
                                                    $offer_name = $arResult["NAME"];
                                                endif;?>
                                                <?$APPLICATION->IncludeComponent("altop:ask.price", "",
                                                    Array(
                                                        "ELEMENT_ID" => $arOffer["ID"],
                                                        "ELEMENT_NAME" => $offer_name,
                                                        "EMAIL_TO" => "",
                                                        "REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL")
                                                    ),
                                                    false
                                                );?>
                                            </div>
                                        <?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                            <div id="detail_price_<?=$arOffer["ID"]?>" class="detail_price <?=$arResult["ID"]?> hidden">
														<span class="OldPrice catalog-detail-item-price-old">
															<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
                                                            <?=$currency;?>
														</span>
														<span class="Price catalog-detail-item-price-new" itemprop="price">
															<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
                                                            <?=$currency;?>
														</span>
                                            </div>
                                        <?else:?>
                                            <div id="detail_price_<?=$arOffer["ID"]?>" class="detail_price <?=$arResult["ID"]?> hidden">
														<span class="Price catalog-detail-item-price" itemprop="price">
															<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
                                                            <?=$currency;?>
														</span>
                                            </div>
                                        <?endif;
                                    endif;
                                endif;
                            endforeach;
                        endforeach;
                    else:
                        foreach($arResult["PRICES"] as $code => $arPrice):
                            if($arPrice["MIN_PRICE"] == "Y"):
                                if($arPrice["CAN_ACCESS"]):

                                    $price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
                                    if(empty($price["THOUSANDS_SEP"])):
                                        $price["THOUSANDS_SEP"] = " ";
                                    endif;
                                    $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                    if($arPrice["VALUE"] == 0):
                                        $arResult["ASK_PRICE"]=1;?>
                                        <meta content="<?=GetMessage('CATALOG_ASK_PRICE')?>" itemprop="price">
                                        <div style="float:left;">
                                            <a class="ask_price_anch" id="ask_price_anch_<?=$arResult['ID']?>" href="#"><?=GetMessage('CATALOG_ASK_PRICE')?></a>
                                            <?$APPLICATION->IncludeComponent("altop:ask.price", "",
                                                Array(
                                                    "ELEMENT_ID" => $arResult["ID"],
                                                    "ELEMENT_NAME" => $arResult["NAME"],
                                                    "EMAIL_TO" => "",
                                                    "REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL")
                                                ),
                                                false
                                            );?>
                                        </div>
                                    <?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                        <span class="OldPrice catalog-detail-item-price-old">
													<span><?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?></span>
                                            <?=$currency;?>
												</span>
                                        <span  class="Price catalog-detail-item-price-new" itemprop="price">
													<span data-price="<?=$arPrice["DISCOUNT_VALUE"]?>"><?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?></span>
                                            <?=$currency;?>
												</span>
                                    <?else:?>
                                        <span class="Price catalog-detail-item-price" itemprop="price">
													<span data-price="<?=$arPrice["VALUE"]?>"><?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?></span>
                                            <?=$currency;?>
												</span>
                                    <?endif;
                                endif;
                            endif;
                        endforeach;
                    endif;?>
                </div>
                <?if((isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) || (isset($arResult["SELECT_PROPS"]) && !empty($arResult["SELECT_PROPS"]))):
                    if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):?>
                        <div class="Availability"><i class="Yes"></i> В наличии</div>
                    <?else:?>
                        <?if($arResult["CAN_BUY"]):
                            if($arResult["ASK_PRICE"]):?>
                                <div class="Availability"><i class="No"></i> Нет в наличии</div>
                            <?elseif(!$arResult["ASK_PRICE"]):?>
                                <div class="Availability"><i class="Yes"></i> В наличии</div>
                            <?endif;
                        elseif(!$arResult["CAN_BUY"]):?>
                            <div class="Availability"><i class="No"></i> Нет в наличии</div>
                        <?endif;
                    endif;
                else:
                    if($arResult["CAN_BUY"]):
                        if($arResult["ASK_PRICE"]):?>
                            <div class="Availability"><i class="No"></i> Нет в наличии</div>
                        <?elseif(!$arResult["ASK_PRICE"]):?>
                            <div class="Availability"><i class="Yes"></i> В наличии</div>
                        <?endif;
                    elseif(!$arResult["CAN_BUY"]):?>
                        <div class="Availability"><i class="No"></i> Нет в наличии</div>
                    <?endif;
                endif;?>
                <hr />
                <?if($arResult['SERVICES']):?>
                    <div class="Services">
                        <div class="Title">Дополнительные услуги:</div>
                        <div class="Items">
                            <?foreach($arResult['SERVICES'] as $arService):?>
                                <div class="Item">
                                    <input type="checkbox" id="<?=$arService['CODE']
                                    ?>" name="service[<?=$arService['CODE']
                                    ?>]" value="Y" data-price="<?=$arService['VALUE']?>">
                                    <label for="<?=$arService['CODE']?>"><span><?=$arService['NAME']?></span></label>
                                    <?if(floatval($arService['VALUE']) > 0):?>
                                        <div class="Price"><span><?=number_format($arService['VALUE'], 0, ',', ' ')?></span> руб.</div>
                                    <?endif;?>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                    foreach($arResult["OFFERS"] as $key => $arOffer):?>
                        <div id="buy_more_detail_<?=$arOffer["ID"]?>" class="buy_more_detail <?=$arResult["ID"]?> hidden">
                            <?if($arOffer["CAN_BUY"]):?>
                                <meta content="InStock" itemprop="availability">
                                <?foreach($arOffer["PRICES"] as $code => $arPrice):
                                    if($arPrice["MIN_PRICE"] == "Y"):
                                        if($arPrice["VALUE"] == 0):?>
                                        <?else:?>
                                            <div id="add2basket_<?=$arOffer["ID"]?>" class="add2basket_block">
                                                <form action="<?=SITE_DIR?>ajax/add2basket.php?action=productAdd" class="add2basket_form" id="add2basket_form_<?=$arOffer["ID"]?>">
                                                    <div class="CountBlock">
                                                        <span>Количество</span>
                                                        <div class="Count">
                                                            <a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_<?=$arOffer["ID"]?>').value > <?=$arOffer["CATALOG_MEASURE_RATIO"]?>) BX('quantity_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_<?=$arOffer["ID"]?>').value)-<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
                                                            <input type="text" id="quantity_<?=$arOffer["ID"]?>" name="quantity" class="quantity" value="<?=$arOffer["CATALOG_MEASURE_RATIO"]?>"/>
                                                            <a href="javascript:void(0)" class="plus" onclick="BX('quantity_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_<?=$arOffer["ID"]?>').value)+<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="ID" value="<?=$arOffer["ID"]?>" />
                                                    <?$props = array();
                                                    foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                        $props[] = array(
                                                            "NAME" => $propOffer["NAME"],
                                                            "CODE" => $propOffer["CODE"],
                                                            "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                        );
                                                    }
                                                    $props = strtr(base64_encode(addslashes(gzcompress(serialize($props),9))), '+/=', '-_,');?>
                                                    <input type="hidden" name="PROPS" value="<?=$props?>" />
                                                    <?if(!empty($arOffer["PREVIEW_IMG"]["SRC"])):?>
                                                        <input type="hidden" name="item_image" id="item_image" value="<?=$arOffer['PREVIEW_IMG']['SRC']?>"/>
                                                    <?else:?>
                                                        <input type="hidden" name="item_image" id="item_image" value="<?=$arResult['PREVIEW_IMG']['SRC']?>"/>
                                                    <?endif;?>
                                                    <input type="hidden" name="item_title" id="item_title" value="<?=$arResult["NAME"]?>"/>
                                                    <input type="hidden" name="item_articul" id="item_articul" value="<?=$arResult['PROPERTIES']['ARTNUMBER']['VALUE']?>"/>
                                                    <input type="hidden" name="item_desc" id="item_desc" value="<?=strip_tags($arResult["PREVIEW_TEXT"]);?>"/>
                                                    <input type="submit" name="add2basket" id="add2basket" value="<?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?>"/>
                                                    <small class="result hidden"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></small>
                                                </form>
                                            </div>
                                            <div class="clr"></div>
                                            <a id="boc_anch_<?=$arOffer['ID']?>" class="boc_anch" href="#"></a>
                                            <?$APPLICATION->IncludeComponent("altop:buy.one.click", ".default",
                                                array(
                                                    "IBLOCK_TYPE" => "catalog",
                                                    "IBLOCK_ID" => "3",
                                                    "ELEMENT_ID" => $arOffer["ID"],
                                                    "ELEMENT_PROPS" => $props,
                                                    "REQUIRED_ORDER_FIELDS" => array(
                                                        0 => "NAME",
                                                        1 => "TEL",
                                                    ),
                                                    "DEFAULT_PERSON_TYPE" => "1",
                                                    "DEFAULT_DELIVERY" => "0",
                                                    "DEFAULT_PAYMENT" => "0",
                                                    "DEFAULT_CURRENCY" => "RUB",
                                                    "BUY_MODE" => "ONE",
                                                    "PRICE_ID" => "1",
                                                    "DUPLICATE_LETTER_TO_EMAILS" => array(
                                                        0 => "a",
                                                    ),
                                                ),
                                                false
                                            );?>
                                        <?endif;
                                    endif;
                                endforeach;
                            elseif(!$arOffer["CAN_BUY"]):?>
                            <?endif;?>
                        </div>
                    <?endforeach;
                else:?>
                    <div class="CountBlock buy_more_detail">
                        <?if($arResult["CAN_BUY"]):?>
                            <meta content="InStock" itemprop="availability">
                            <?if($arResult["ASK_PRICE"]):?>
                                <div id="available"></div>
                            <?elseif(!$arResult["ASK_PRICE"]):?>
                                <div id="add2basket_<?=$arResult["ID"]?>" class="add2basket_block">
                                    <form action="<?=SITE_DIR?>ajax/action_basket.php?action=productAdd" class="add2basket_form" id="add2basket_form_<?=$arResult["ID"]?>">
                                        <div class="CountBlock">
                                            <span>Количество</span>
                                            <div class="Count">
                                                <a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_<?=$arResult["ID"]?>').value > <?=$arResult["CATALOG_MEASURE_RATIO"]?>) BX('quantity_<?=$arResult["ID"]?>').value = parseFloat(BX('quantity_<?=$arResult["ID"]?>').value)-<?=$arResult["CATALOG_MEASURE_RATIO"]?>;"></a>
                                                <input type="text" id="quantity_<?=$arResult["ID"]?>" name="quantity" class="quantity" value="<?=$arResult["CATALOG_MEASURE_RATIO"]?>"/>
                                                <a href="javascript:void(0)" class="plus" onclick="BX('quantity_<?=$arResult["ID"]?>').value = parseFloat(BX('quantity_<?=$arResult["ID"]?>').value)+<?=$arResult["CATALOG_MEASURE_RATIO"]?>;"></a>
                                            </div>
                                        </div>
                                        <input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
                                        <input type="hidden" name="item_image" id="item_image" value="<?=$arResult["PREVIEW_IMG"]['SRC']?>"/>
                                        <input type="hidden" name="item_title" id="item_title" value="<?=$arResult["NAME"]?>"/>
                                        <input type="hidden" name="item_articul" id="item_articul" value="<?=$arResult['PROPERTIES']['ARTNUMBER']['VALUE']?>"/>
                                        <input type="hidden" name="item_desc" id="item_desc" value="<?=strip_tags($arResult["PREVIEW_TEXT"]);?>"/>
                                        <input type="submit" name="add2basket" id="add2basket" value="<?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?>"/>
                                        <small class="result hidden"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></small>
                                    </form>
                                </div>
                                <div class="clr"></div>
                                <a id="boc_anch_<?=$arResult['ID']?>" class="BuyClick boc_anch" href="#"><?=GetMessage("CATALOG_BUY_CLICK")?></a>
                                <?$APPLICATION->IncludeComponent("altop:buy.one.click", ".default",
                                    array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "14",
                                        "ELEMENT_ID" => $arResult["ID"],
                                        "ELEMENT_PROPS" => "",
                                        "REQUIRED_ORDER_FIELDS" => array(
                                            0 => "NAME",
                                            1 => "TEL",
                                        ),
                                        "DEFAULT_PERSON_TYPE" => "1",
                                        "DEFAULT_DELIVERY" => "0",
                                        "DEFAULT_PAYMENT" => "0",
                                        "DEFAULT_CURRENCY" => "RUB",
                                        "BUY_MODE" => "ONE",
                                        "PRICE_ID" => "1",
                                        "DUPLICATE_LETTER_TO_EMAILS" => array(
                                            0 => "a",
                                        ),
                                    ),
                                    false
                                );?>
                            <?endif;?>
                        <?elseif(!$arResult["CAN_BUY"]):?>
                            <meta content="OutOfStock" itemprop="availability">
                            <div id="not_available"></div>
                        <?endif;?>
                    </div>
                <?endif;?>
            </div>
            <?$this->EndViewTarget();?>
            <div class="catalog-detail-info">
                <div class="rating_compare_detail">
                    <div class="Props">
                        <?if($arResult["PROPERTIES"]["BREND"]["VALUE"]):?>
                            <?$res = CIBlockElement::GetByID($arResult["PROPERTIES"]["BREND"]["VALUE"]);
                            if($ar_res = $res->GetNext())
                                echo "<div class=\"Item\"><div class=\"Name\">Производитель</div><div class=\"Value\">".$ar_res['NAME']."</div></div>";
                            ?>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"])):?>
                            <?if($arResult["DISPLAY_PROPERTIES"]["PROPS"]["VALUE"]):?>
                                <?
								$i = 0;
								foreach($arResult["DISPLAY_PROPERTIES"]["PROPS"]["VALUE"] as $k=>$value){?>
                                    <div class="Item">
                                        <div class="Name"><?=$value?></div>
                                        <div class="Value"><?=$arResult["DISPLAY_PROPERTIES"]["PROPS"]["DESCRIPTION"][$k]?></div>
                                    </div>
                                <?
								if (++$i == 11) {
									break;
								}}
								?>
                            <?endif;?>
                        <?endif;?>
                        <div class="Links ">
                            <a href="<?=$arResult["DETAIL_PAGE_URL"]?>#tab2" class="AllProps Close">Все характеристики <i></i></a>
                            <div class="Icons">
                                <?if($arParams["USE_COMPARE"]=="Y"):?>
                                    <div id="catalog-compare" class="AddCompare">
                                        <a href="<?=$arResult["COMPARE_URL"]?>" class="catalog-item-compare" onclick="return addToCompare(this);" id="catalog_add2compare_link_<?=$arResult['ID']?>">
                                            <span class="add"><i></i></span>
                                            <span class="added"><i></i></span>
                                        </a>
                                    </div>
                                <?endif;
                                if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                                    foreach($arResult["OFFERS"] as $key => $arOffer):
                                        if($arOffer["CAN_BUY"]):
                                            foreach($arOffer["PRICES"] as $code => $arPrice):
                                                if($arPrice["MIN_PRICE"] == "Y"):
                                                    if($arPrice["VALUE"] > 0):
                                                        $props = array();
                                                        foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                            $props[] = array(
                                                                "NAME" => $propOffer["NAME"],
                                                                "CODE" => $propOffer["CODE"],
                                                                "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                            );
                                                        }
                                                        $props = strtr(base64_encode(addslashes(gzcompress(serialize($props),9))), '+/=', '-_,');?>
                                                        <div id="add_to_delay_<?=$arOffer["ID"]?>" class="AddFavorite add_to_delay <?=$arResult["ID"]?> hidden">
                                                            <a href="javascript:void(0)" id="catalog-item-delay-<?=$arOffer['ID']?>" class="catalog-item-delay" onclick="return addToDelay('<?=$arOffer["ID"]?>', '<?=$arOffer["CATALOG_MEASURE_RATIO"]?>', '<?=$props?>', 'catalog-item-delay-<?=$arOffer["ID"]?>')">
                                                                <span class="add"><i></i></span>
                                                                <span class="added"><i></i></span>
                                                            </a>
                                                        </div>
                                                    <?endif;
                                                endif;
                                            endforeach;
                                        endif;
                                    endforeach;
                                else:
                                    if($arResult["CAN_BUY"]):
                                        foreach($arResult["PRICES"] as $code=>$arPrice):
                                            if($arPrice["MIN_PRICE"] == "Y"):
                                                if($arPrice["VALUE"] > 0):?>
                                                    <div class="AddFavorite add_to_delay">
                                                        <a href="javascript:void(0)" id="catalog-item-delay-<?=$arResult['ID']?>" class="catalog-item-delay" onclick="return addToDelay('<?=$arResult["ID"]?>', '<?=$arResult["CATALOG_MEASURE_RATIO"]?>', '', 'catalog-item-delay-<?=$arResult["ID"]?>')">
                                                            <span class="add"><i></i></span>
                                                            <span class="added"><i></i></span>
                                                        </a>
                                                    </div>
                                                <?endif;
                                            endif;
                                        endforeach;
                                    endif;
                                endif?>
                            </div>
                        </div>
                    </div>
                </div>
                <?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                    if(count($arResult['SKU_PROPERTIES']) > 0 && count($arResult['SKU_PROPERTIES']) < 3):?>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                <?foreach($arResult['SKU_PROPERTIES'] as $keyProp => $sku_props):
                                    $keys = array_keys($arResult['SKU_PROPERTIES']);
                                    $current_key_index = array_search($keyProp, $keys);
                                    $prev_key = false;
                                    $next_key = false;
                                    if(isset($keys[$current_key_index + -1]))
                                        $prev_key = $keys[$current_key_index + -1];
                                    if(isset($keys[$current_key_index + 1]))
                                        $next_key = $keys[$current_key_index + 1];?>

                                <?if($prev_key == false && $next_key == false) {?>
                                $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> span").first().addClass("active selected");
                                rel = $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> span.active.selected").attr("id");
                                $("#detail_picture_"+rel).removeClass("hidden");
                                $("#detail_price_"+rel).removeClass("hidden");
                                $("#buy_more_detail_"+rel).removeClass("hidden");
                                $("#add_to_delay_"+rel).removeClass("hidden");
                                <?} elseif($prev_key == false && $next_key != false) {?>
                                $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> span").first().addClass("active selected");
                                rel = $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> span.active.selected").attr("rel");
                                $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li").hide();
                                $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+rel+"]").show();
                                $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+rel+"] span").first().addClass("active selected");
                                rel2 = $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+rel+"] span.active.selected").attr("id");
                                $("#detail_picture_"+rel2).removeClass("hidden");
                                $("#detail_price_"+rel2).removeClass("hidden");
                                $("#buy_more_detail_"+rel2).removeClass("hidden");
                                $("#add_to_delay_"+rel2).removeClass("hidden");
                                <?}?>

                                $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span").bind({
                                    click: function(){
                                        var t = $(this);
                                        <?if($prev_key == false && $next_key == false) {?>
                                        id = t.attr("id");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span").removeClass("active selected");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span[id="+id+"]").addClass("active selected");
                                        $(".detail_picture.<?=$arResult['ID']?>, .detail_price.<?=$arResult['ID']?>, .buy_more_detail.<?=$arResult['ID']?>, .add_to_delay.<?=$arResult['ID']?>").addClass("hidden");
                                        $("#detail_picture_"+id).removeClass("hidden");
                                        $("#detail_price_"+id).removeClass("hidden");
                                        $("#buy_more_detail_"+id).removeClass("hidden");
                                        $("#add_to_delay_"+id).removeClass("hidden");
                                        <?} elseif($prev_key == false && $next_key != false) {?>
                                        id = t.attr("rel");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span").removeClass("active selected");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span[rel="+id+"]").addClass("active selected");
                                        $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li").hide();
                                        $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+id+"]").show();
                                        $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+id+"] span").removeClass("active selected");
                                        $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+id+"] span").first().addClass("active selected");
                                        id2 = $("ul.<?=$arResult['ID']?>.<?=$arResult['SKU_PROPERTIES'][$next_key]['CODE']?> li[rel="+id+"] span.active.selected").attr("id");
                                        $(".detail_picture.<?=$arResult['ID']?>, .detail_price.<?=$arResult['ID']?>, .buy_more_detail.<?=$arResult['ID']?>, .add_to_delay.<?=$arResult['ID']?>").addClass("hidden");
                                        $("#detail_picture_"+id2).removeClass("hidden");
                                        $("#detail_price_"+id2).removeClass("hidden");
                                        $("#buy_more_detail_"+id2).removeClass("hidden");
                                        $("#add_to_delay_"+id2).removeClass("hidden");
                                        <?} elseif($prev_key != false && $next_key == false) {?>
                                        id = t.attr("id");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span").removeClass("active selected");
                                        $("ul.<?=$arResult['ID']?>.<?=$sku_props['CODE']?> li span[id="+id+"]").addClass("active selected");
                                        $(".detail_picture.<?=$arResult['ID']?>, .detail_price.<?=$arResult['ID']?>, .buy_more_detail.<?=$arResult['ID']?>, .add_to_delay.<?=$arResult['ID']?>").addClass("hidden");
                                        $("#detail_picture_"+id).removeClass("hidden");
                                        $("#detail_price_"+id).removeClass("hidden");
                                        $("#buy_more_detail_"+id).removeClass("hidden");
                                        $("#add_to_delay_"+id).removeClass("hidden");
                                        <?}?>
                                        return false;
                                    }
                                });
                                <?endforeach;?>
                            });
                        </script>

                    <?foreach($arResult["SKU_PROPERTIES"] as $sku_props):?>
                        <div class="offer_block">
                            <div class="h3"><?=$sku_props['NAME']?></div>
                            <?$props = array_map("unserialize", array_unique(array_map("serialize", $sku_props["VALUES"])));?>
                            <ul id="<?=$arResult['ID']?>" class="<?=$arResult['ID'].' '.$sku_props['CODE']?>">
                                <?foreach($props as $val):
                                    if('COLOR' == $sku_props['CODE']):?>
                                        <li <?if(!empty($val["PARENT_ID"])): echo 'rel="'.$val["PARENT_ID"].'"'; endif?>>
                                            <?if(!empty($val["PICT"]["src"])):?>
                                                <span <?if(!empty($val["ID"])): echo 'rel="'.$val["ID"].'"'; endif;?> <?if(!empty($val["OFFER_ID"])): echo "id='".$val["OFFER_ID"]."'"; endif;?> title="<?=$val['VALUE']?>">
																<img src="<?=$val['PICT']['src']?>" width="<?=$val['PICT']['width']?>" height="<?=$val['PICT']['height']?>" />
															</span>
                                            <?else:?>
                                                <span <?if(!empty($val["ID"])): echo 'rel="'.$val["ID"].'"'; endif;?> <?if(!empty($val["OFFER_ID"])): echo "id='".$val["OFFER_ID"]."'"; endif;?> title="<?=$val['VALUE']?>">
																<i style="background:#<?=$val['HEX']?>"></i>
															</span>
                                            <?endif;?>
                                        </li>
                                    <?else:?>
                                        <li <?if(!empty($val["PARENT_ID"])): echo 'rel="'.$val["PARENT_ID"].'"'; endif?>>
                                            <span <?if(!empty($val["ID"])): echo 'rel="'.$val["ID"].'"'; endif;?> <?if(!empty($val["OFFER_ID"])): echo "id='".$val["OFFER_ID"]."'"; endif;?>><?=$val["VALUE"]?></span>
                                        </li>
                                    <?endif;
                                endforeach;?>
                            </ul>
                            <div class="clr"></div>
                        </div>
                    <?endforeach;
                    elseif(count($arResult['SKU_PROPERTIES']) > 0 && count($arResult['SKU_PROPERTIES']) >= 3):?>
                        <div class="errortext" style="float:left; margin:10px 0px 0px 0px;">
                            <?=GetMessage('CATALOG_ELEMENT_TOO_MANY_PROPERTIES')?>
                        </div>
                    <?endif;
                endif;?>
            </div>
            <?$frame->end();?>
        </div>

         <div class="TabsInfo">
            <ul class="TabsLink">
                <li class="current"><?=GetMessage('CATALOG_FULL_DESCRIPTION')?></li>
                <li id="tab2"><?=GetMessage('CATALOG_PROPERTIES')?></li>
                <li style="<?if(empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])){ echo 'display:none;'; }?>"><?=$arResult["PROPERTIES"]["ACCESSORIES"]["NAME"]?></li>
                <li><?=GetMessage('DELIVERY')?></li>
            </ul>
            <div class="BlockBox visible">
                <?if(!empty($arResult["PREVIEW_TEXT"])):?>
				<div class="Desc">
                    <?=$arResult["PREVIEW_TEXT"]?>
				</div>
                <?endif;?>
            </div>
            <div class="BlockBox">
                <div class="Props">
                    <?if($arResult["PROPERTIES"]["BREND"]["VALUE"]):?>
                        <?$res = CIBlockElement::GetByID($arResult["PROPERTIES"]["BREND"]["VALUE"]);
                        if($ar_res = $res->GetNext())
                            echo "<div class=\"Item\"><div class=\"Name\">Производитель</div><div class=\"Value\">".$ar_res['NAME']."</div></div>";
                        ?>
                    <?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"])):?>
                        <?if($arResult["DISPLAY_PROPERTIES"]["PROPS"]["VALUE"]):?>
                            <?foreach($arResult["DISPLAY_PROPERTIES"]["PROPS"]["VALUE"] as $k=>$value):?>
                                <div class="Item">
                                    <div class="Name"><?=$value?></div>
                                    <div class="Value"><?=$arResult["DISPLAY_PROPERTIES"]["PROPS"]["DESCRIPTION"][$k]?></div>
                                </div>
                            <?endforeach?>
                        <?endif;?>
                    <?endif;?>
                </div>
            </div>
            <div class="BlockBox" style="<?if(empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])){ echo 'display:none;'; }?>">
                <div class="accessories">
                    <?/*$arProperty = $arResult["PROPERTIES"]["ACCESSORIES"]["PROPERTY_VALUE_ID"];
				if($arProperty !=""): 
					global $arRecPrFilter;
					
					$arRecPrFilter["ID"] = $arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"];
					$APPLICATION->IncludeComponent("artex:catalog.top", "access",
						Array(
							"DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
							"DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
							"SHARPEN" => $arParams["SHARPEN"],
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => "rand",
							"ELEMENT_SORT_ORDER" => "asc",
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"ELEMENT_COUNT" => "6",
							"LINE_ELEMENT_COUNT" => "",
							"FILTER_NAME" => "arRecPrFilter",
							"PROPERTY_CODE" => array("NEWPRODUCT", "SALELEADER", "DISCOUNT", "MANUFACTURER"),
							"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"HIDE_NOT_AVAILABLE" => "N",
							"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
							"CURRENCY_ID" => $arParams['CURRENCY_ID'],
							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]
						),
						false
					);
					unset($arResult["PROPERTIES"]["ACCESSORIES"]);
				endif;*/?>
                </div>
            </div>
            <div class="BlockBox">
                Доставка осуществляется в любой регион России, доступный для почтовых и транспортных отправлений.
            </div>
        </div>
       <div class="Clear"></div>
        <?/*$APPLICATION->IncludeComponent(
		"bitrix:sale.viewed.product", 
		"viewed.product", 
		array(
			"VIEWED_COUNT" => "15",
			"VIEWED_NAME" => "Y",
			"VIEWED_IMAGE" => "Y",
			"VIEWED_PRICE" => "Y",
			"VIEWED_CANBUY" => "N",
			"VIEWED_CANBUSKET" => "Y",
			"VIEWED_IMG_HEIGHT" => "166",
			"VIEWED_IMG_WIDTH" => "140",
			"BASKET_URL" => "/personal/cart/",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"SET_TITLE" => "N",
			"COMPONENT_TEMPLATE" => "viewed.product",
			"VIEWED_CURRENCY" => "default",
			"VIEWED_CANBASKET" => "Y"
		),
		false
	);*/?>
        <div class="clr"></div>
    </div>
<?/*global $arRelPrFilter;
$arRelPrFilter = Array("SECTION_ID" => $arResult["IBLOCK_SECTION_ID"], "!ID" => $arResult["ID"]);
$APPLICATION->IncludeComponent("altop:catalog.top", "related",
	Array(
		"DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
		"DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
		"SHARPEN" => $arParams["SHARPEN"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => "rand",
		"ELEMENT_SORT_ORDER" => "asc",
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"ELEMENT_COUNT" => "4",
		"LINE_ELEMENT_COUNT" => "",
		"FILTER_NAME" => "arRelPrFilter",
		"PROPERTY_CODE" => array("NEWPRODUCT", "SALELEADER", "DISCOUNT", "MANUFACTURER"),
		"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams['CURRENCY_ID'],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]
	),
	false
);*/?>