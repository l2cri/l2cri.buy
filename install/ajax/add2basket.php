<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(CModule::IncludeModule('l2cri.buy'))
{
    // default
	$action = array_key_exists('action',$_GET) ? $_GET['action']: 'productAdd';
	// only add product with array props
	if($action == 'productAdd'){

		$product_id = intval($_REQUEST['ID']);
		$quantity = intval($_REQUEST['quantity']);
		$arServices = array_keys($_REQUEST['service']);

		echo L2cri\Buy\Product::add($product_id,$quantity,$arServices);
	}
	// add new prop in basket position
	if($action == 'propAdd'){

		$productID = intval($_REQUEST['product_id']);
		$serviceID = intval($_REQUEST['prop_id']);
		$basketID = intval($_REQUEST['basket_id']);

		echo L2cri\Buy\Services::add($productID,$serviceID,$basketID);
	}
	// del prop from basket position
	if($action == 'propDel'){

		$productID = intval($_REQUEST['product_id']);
		$serviceID = intval($_REQUEST['prop_id']);
		$basketID = intval($_REQUEST['basket_id']);

		echo L2cri\Buy\Services::del($productID,$serviceID,$basketID);
	}
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>